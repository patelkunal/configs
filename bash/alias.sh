alias gs='git status'
alias ga='git add'
alias gt='git'
alias got='git'
alias gut='git'
alias venv='virtualenv'

alias l='ls'
alias ll='ls -lrth'
alias lla='ls -lrha'
alias la='ls -a'
alias ..='cd ..'

